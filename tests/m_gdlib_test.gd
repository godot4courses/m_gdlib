extends Node

var is_initialized := false
var m_gdlib: MGdLib
var _dialog_is_modal := true
var _avoid_pressed_propagation := false  # used when changing the value of %isModal checkbox without bubbling its pressed signal


func _ready() -> void:
	if !is_instance_valid(m_gdlib):
		m_gdlib = MGdLib.new()

	if is_instance_of(m_gdlib, MGdLib):
		m_gdlib.init_dialog_box(get_tree().root)
		is_initialized = m_gdlib.is_initialized
		_signals_connect()


func _signals_connect() -> void:
	for child in $VSeparator.get_children():
		if child is Button:
			var error := (child as Button).pressed.connect(_on_button_pressed.bind(child))
			if error:
				printerr("Enable to connect the signal for the button %s" % child.name)


func _on_button_pressed(button: Button) -> void:
	if is_initialized:
		_change_modal_state(m_gdlib.dialog_box.state == MGDLIB_DialogBox.BoxState.MODAL)
	var key := button.name.replace("Open", "")
	match key:
		"INFO", "DEFAULT":
			m_gdlib.dialog_box.message("A info dialog has been opened")
		"DEBUG":
			m_gdlib.dialog_box.debug("A debug dialog has been opened")
		"WARNING":
			m_gdlib.dialog_box.warning("A warning dialog has been opened")
		"ERROR":
			m_gdlib.dialog_box.error("A error dialog has been opened")
		"FATAL":
			m_gdlib.dialog_box.fatal("A Fatal dialog has been opened")


func _on_hide_pressed() -> void:
	if m_gdlib.dialog_box.is_initialized:
		m_gdlib.dialog_box.hide()
	else:
		MGDLIB_DialogBox.internal_error(m_gdlib.ErrorCode.NOT_INITIALIZED)


func _on_show_pressed() -> void:
	if m_gdlib.dialog_box.is_initialized:
		m_gdlib.dialog_box.show()
	else:
		MGDLIB_DialogBox.internal_error(m_gdlib.ErrorCode.NOT_INITIALIZED)


func _on_close_pressed() -> void:
	if m_gdlib.dialog_box.is_initialized:
		m_gdlib.dialog_box.close()
	else:
		MGDLIB_DialogBox.internal_error(m_gdlib.ErrorCode.NOT_INITIALIZED)


func _on_is_modal_toggled(toggled_on: bool) -> void:
	if _avoid_pressed_propagation:
		return
	_dialog_is_modal = toggled_on
	if _dialog_is_modal:
		m_gdlib.dialog_box.state = MGDLIB_DialogBox.BoxState.MODAL
	else:
		m_gdlib.dialog_box.state = MGDLIB_DialogBox.BoxState.NOT_MODAL

	# the following code add a check for the dialog box to be initialized BEFORE changing the checkbutton state
	#if m_gdlib.dialog_box.is_initialized:
	#_dialog_is_modal = toggled_on
	#if _dialog_is_modal:
	#m_gdlib.dialog_box.state = MGDLIB_DialogBox.BoxState.MODAL
	#else:
	#m_gdlib.dialog_box.state = MGDLIB_DialogBox.BoxState.NOT_MODAL
	#else:
	#DialogBox.internal_error(m_gdlib.ErrorCode.NOT_INITIALIZED)
	## restore the state of the button
	#_change_modal_state(!(%isModal as CheckButton).button_pressed)


func _change_modal_state(new_value: bool) -> void:
	_avoid_pressed_propagation = true
	(%isModal as CheckButton).button_pressed = new_value
	_avoid_pressed_propagation = false
