# MGdlib and TEST PROJECT

![large_icon](https://gitlab.com/godot4courses/m_gdlib/raw/master/addons/m_gdlib/assets/large_icon.png)

## Description

MGdLib: a GDscript utility Library for Godot 4.
and a project to test the MGdLib

## Copyrights

Coded in GDscript using the Godot Engine game engine.
Developed using mainly the integrated code editor and/or Visual Studio Code.

-----------------
(C) Laurent Ongaro / GameaMea Studio (https://www.gameamea.com)
