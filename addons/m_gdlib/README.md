# MGdlib

![large_icon](https://gitlab.com/godot4courses/m_gdlib/raw/master/addons/m_gdlib/assets/large_icon.png)

## Description

Une bibliothèque utilitaire GDScript pour Godot 4.

## Copyrights

Codé en GDscript en utilisant le moteur de jeu Godot Engine.
Développé en utilisant principalement l'éditeur de code intégré et/ou Visual Studio Code.

-----------------
(C) GameaMea Studio (https://www.gameamea.com)

=================================================

## Description

A GDscript utility Library for Godot 4.

## Copyrights

Coded in GDscript using the Godot Engine game engine.
Developed using mainly the integrated code editor and/or Visual Studio Code.

-----------------
(C) GameaMea Studio (https://www.gameamea.com)
