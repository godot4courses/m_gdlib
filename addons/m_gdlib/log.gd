##A Fake Log Class to be used if the "real" Log class in not present in the project
##If the logger addons is not present, you should add this file as an autoload and use "Log" for its name

class_name MGDLIB_LogFake extends Node


static func info(message: String) -> void:
	print(message)


static func dbg(message: String) -> void:
	print(message)


static func warn(message: String) -> void:
	print(message)


static func error(message: String) -> void:
	print(message)


static func fatal(message: String) -> void:
	print(message)
